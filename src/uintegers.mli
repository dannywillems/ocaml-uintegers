module Uint64 : sig
  (** Encode a unsigned uint64 *)
  type t

  val zero : t

  val one : t

  val add : t -> t -> t

  val eq : t -> t -> bool

  val copy : t -> t

  val random : ?state:Random.State.t -> unit -> t

  val of_bytes_le : Bytes.t -> t

  val to_bytes_le : t -> Bytes.t

  val to_z : t -> Z.t
end

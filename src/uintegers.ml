module Stubs = struct
  (* type c_uint32 *)

  type c_uint64

  (* external uint32_add : c_uint32 -> c_uint32 -> c_uint32 = "caml_uintegers_uint32_add_stubs" *)

  (* external uint32_zero : c_uint32 = "caml_uintegers_uint32_zero_stubs" *)

  (* external uint32_one : c_uint32 = "caml_uintegers_uint32_one_stubs" *)

  external uint64_allocate : unit -> c_uint64
    = "caml_uintegers_uint64_allocate_stubs"

  external uint64_add : c_uint64 -> c_uint64 -> c_uint64
    = "caml_uintegers_uint64_add_stubs"

  external uint64_copy : c_uint64 -> c_uint64
    = "caml_uintegers_uint64_copy_stubs"

  external uint64_eq : c_uint64 -> c_uint64 -> bool
    = "caml_uintegers_uint64_eq_stubs"

  external uint64_zero : unit -> c_uint64 = "caml_uintegers_uint64_zero_stubs"

  external uint64_one : unit -> c_uint64 = "caml_uintegers_uint64_one_stubs"

  external uint64_set_bytes : c_uint64 -> Bytes.t -> unit
    = "caml_uintegers_uint64_set_bytes_stubs"

  external uint64_get_bytes_le : c_uint64 -> Bytes.t -> unit
    = "caml_uintegers_uint64_get_bytes_le_stubs"
end

(* module Uint32 = struct *)
(*   type t = Stubs.c_uint32 *)

(*   let add = Stubs.uint32_add *)

(*   let one = Stubs.uint32_one *)
(* end *)

module Uint64 = struct
  type t = Stubs.c_uint64

  let add = Stubs.uint64_add

  let zero = Stubs.uint64_zero ()

  let one = Stubs.uint64_one ()

  let eq = Stubs.uint64_eq

  let allocate () = Stubs.uint64_allocate ()

  let copy x = Stubs.uint64_copy x

  let size_in_bytes = 8

  let of_bytes_le bs =
    let bs_length = Bytes.length bs in
    (* We assert we have less than 8 bytes *)
    assert (bs_length <= 8);
    (* We pad to have 64 bits, i.e. 8 bytes *)
    let padded_bs = Bytes.make 8 '\000' in
    Bytes.blit bs 0 padded_bs 0 bs_length ;
    let buffer = allocate () in
    Stubs.uint64_set_bytes buffer padded_bs ;
    buffer

  let random ?state () =
    let random_int =
      match state with
      | None -> Random.int
      | Some state -> Random.State.int state
    in
    let random_bytes =
      Bytes.init size_in_bytes (fun _ -> char_of_int @@ random_int 256)
    in
    of_bytes_le random_bytes

  let to_bytes_le v =
    let bytes = Bytes.make 8 '\000' in
    Stubs.uint64_get_bytes_le v bytes ;
    bytes

  let to_z v =
    let bs = to_bytes_le v in
    Z.of_bits (Bytes.to_string bs)
end

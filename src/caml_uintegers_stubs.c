#include <caml/alloc.h>
#include <caml/custom.h>
#include <caml/fail.h>
#include <caml/memory.h>
#include <caml/mlvalues.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define NB_BYTES_UINT64 8

#define Uintegers_uint64_val(v) (*((uint64_t *)(Data_custom_val(v))))

static struct custom_operations uintegers_uint64_ops = {
    "uintegers_uint64",         custom_finalize_default,
    custom_compare_default,     custom_hash_default,
    custom_serialize_default,   custom_deserialize_default,
    custom_compare_ext_default, custom_fixed_length_default};

CAMLprim value caml_uintegers_uint64_allocate_stubs(value vunit) {
  CAMLparam1(vunit);
  CAMLlocal1(block);
  block = caml_alloc_custom(&uintegers_uint64_ops, NB_BYTES_UINT64, 0, 1);
  CAMLreturn(block);
}

CAMLprim value caml_uintegers_uint64_zero_stubs(value vunit) {
  CAMLparam1(vunit);
  CAMLlocal1(block);
  block = caml_alloc_custom(&uintegers_uint64_ops, NB_BYTES_UINT64, 0, 1);
  memset((uint64_t *)Data_custom_val(block), 0, NB_BYTES_UINT64);
  CAMLreturn(block);
}

CAMLprim value caml_uintegers_uint64_one_stubs(value vunit) {
  CAMLparam1(vunit);
  CAMLlocal1(block);
  block = caml_alloc_custom(&uintegers_uint64_ops, NB_BYTES_UINT64, 0, 1);
  uint64_t *c = (uint64_t *)(Data_custom_val(block));
  *c = (uint64_t)1;
  CAMLreturn(block);
}

CAMLprim value caml_uintegers_uint64_copy_stubs(value vx) {
  CAMLparam1(vx);
  CAMLlocal1(block);
  uint64_t x = Uintegers_uint64_val(vx);
  block = caml_alloc_custom(&uintegers_uint64_ops, NB_BYTES_UINT64, 0, 1);
  memcpy(Data_custom_val(block), &x, NB_BYTES_UINT64);
  CAMLreturn(block);
}

CAMLprim value caml_uintegers_uint64_add_stubs(value vres, value vx, value vy) {
  // FIXME
  CAMLparam3(vres, vx, vy);
  /* uint64_t res = Uintegers_uint64_val(vres); */
  /* uint64_t x = Uintegers_uint64_val(vx); */
  /* uint64_t y = Uintegers_uint64_val(vy); */
  /* res = x + y; */
  CAMLreturn(Val_unit);
}

CAMLprim value caml_uintegers_uint64_eq_stubs(value vx, value vy) {
  CAMLparam2(vx, vy);
  uint64_t x = Uintegers_uint64_val(vx);
  uint64_t y = Uintegers_uint64_val(vy);
  CAMLreturn(Val_bool(x == y));
}

CAMLprim value caml_uintegers_uint64_set_bytes_stubs(value vres, value vbytes) {
  CAMLparam2(vres, vbytes);
  // TODO: add assert vbytes len < 8
  uint64_t res = Uintegers_uint64_val(vres);
  int bytes_len = caml_string_length(vbytes);
  printf("Bytes length: %d\n", bytes_len);
  printf("----------\n");
  for (int i = 0; i < bytes_len; i++) {
    if (i == 0) {
      printf("Current bytes value: %d\n", Byte_u(vbytes, i));
    }
    /* printf("Before: %ld\n", res); */
    res += (1 << (8 * i)) * Byte_u(vbytes, i);
    /* printf("After: %ld\n", res); */
  }
  CAMLreturn(Val_unit);
}

CAMLprim value caml_uintegers_uint64_get_bytes_le_stubs(value vres,
                                                        value vbytes) {
  CAMLparam2(vres, vbytes);
  // TODO: add assert vbytes len < 8
  uint64_t res = Uintegers_uint64_val(vres);
  unsigned char *bs = Bytes_val(vbytes);
  /* int bs_len = caml_string_length(vbytes); */
  /* for (int i = 0; i < bs_len; i++) { */
  bs[0] = 0xff & res;
  printf("bs[0]: %d\n", bs[0]);
  /* bytes[bs_len - 1 - i] =  */
  CAMLreturn(Val_unit);
}

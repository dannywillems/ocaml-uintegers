module TestUint64 = struct
  (* let test_random_add () = *)
  (*   let x = Uintegers.Uint64.random () in *)
  (*   let y = Uintegers.Uint64.random () in *)
  (*   ignore @@ Uintegers.Uint64.add x y ; *)
  (*   assert true *)

  (* let test_one_zero_diff () = *)
  (*   (\* assert (not @@ Uintegers.Uint64.(eq zero one)) ; *\) *)
  (*   assert (not @@ Uintegers.Uint64.(eq one zero)) *)

  let test_zero_zero_eq () = assert (Uintegers.Uint64.(eq zero zero))

  let test_one_one_eq () = assert (Uintegers.Uint64.(eq one one))

  let test_get_bytes () =
    let one_byte = Bytes.make 1 (char_of_int @@ Random.int 256) in
    let v = Uintegers.Uint64.of_bytes_le one_byte in
    assert (Bytes.equal (Uintegers.Uint64.to_bytes_le v) one_byte)

  (* let test_random_zero_eq () = *)
  (*   let r = Uintegers.Uint64.random () in *)
  (*   assert (not @@ Uintegers.Uint64.(eq zero r)) *)

  let test_one_zarith () =
    print_endline "test_one_zarith";
    print_endline (Z.to_string (Uintegers.Uint64.(to_z one)));
    flush_all ();
    assert (Z.equal (Uintegers.Uint64.(to_z one)) Z.one)

  let test_zero_zarith () =
    print_endline "test_zero_zarith";
    print_endline (Z.to_string (Uintegers.Uint64.(to_z zero)));
    flush_all ();
    assert (Z.equal (Uintegers.Uint64.(to_z zero)) Z.zero)

end

let () =
  let open Alcotest in
  run
    "Uintegers"
    [ ( "Properties",
        [ (* test_case "add" `Quick (Utils.repeat 100 test_random_add); *)
          (* test_case *)
          (*   "random zero eq" *)
          (*   `Quick *)
          (*   (Utils.repeat 100 TestUint64.test_random_zero_eq); *)
          test_case
            "zero zarith"
            `Quick
            TestUint64.test_zero_zarith;
          test_case
            "one zarith"
            `Quick
            TestUint64.test_one_zarith;
          test_case
            "get_bytes one_byte "
            `Quick
            (Utils.repeat 100 TestUint64.test_get_bytes);
          test_case
            "zero zero eq"
            `Quick
            TestUint64.test_zero_zero_eq;
          (* test_case "zero one diff" `Quick TestUint64.test_one_zero_diff; *)
          test_case
            "one one eq"
            `Quick
            (Utils.repeat 100 TestUint64.test_one_one_eq) ] ) ]

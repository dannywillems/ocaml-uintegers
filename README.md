# ocaml-uintegers

## Motivation

In OCaml, a uniform memory representation is used, where each OCaml variable is
stored as a value. This value is a single memory word that can be either an
immediate integer or a pointer to another memory location. The OCaml runtime
keeps track of all values to properly deallocate them when they are no longer
required.
To differentiate immedidate integers and pointers to another memory location,
the OCaml runtime encodes this information in a single bit. It implies OCaml
int64 is not a 64 bit integer, but 63 bits. It can be critical when writing
bindings with other languages when mapping integers values as the domain won't
be the same.

<!-- Describe real world bug/use cases -->

Some libraries exist to encode unsigned 64 bits, like
[ocaml-integers](https://github.com/yallop/ocaml-integers).
These libraries use a custom block and allocates a value on the C heap to save
the values. This works, but it creates an indirection and an overhead at runtime
when deallocating or accessing the value.

Values could be saved directly in the custom blocks, as long as it is word
aligned. This is the motivation of this library.

## Memory layout of values

The library uses a custom block and uses a C memory layout to represent the memory allocated values. Here is the mapping:

- `Uint64.t` = `uint64_t` from `stdint.h`

## Installation

```
opam install uintegers
```

## Run tests

```
dune build @runtest
```

## Run benchmark

```
dune exec ./benchmark/uint64_bench.exe
```
